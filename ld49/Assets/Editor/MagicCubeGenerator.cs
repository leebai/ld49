#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class MagicCubeGenerator : MonoBehaviour
{
    [MenuItem("MyMenu/Generate Magic Cube")]
    static void CreateMagicCube()
    {
        loadMeshes();

        float size = m_gameObjects[0].GetComponent<MeshRenderer>().bounds.size.x;
        
        float margin = -0.10f;
        int dimension = 6;
        List<GameObject> cubeUnits = new List<GameObject>();
        GameObject magicCube = generateCubes( size, margin, dimension, out cubeUnits );
        MagicCubeContoller controller = magicCube.AddComponent<MagicCubeContoller>();

        List<List<GameObject>> xs = controller.sortCubes( cubeUnits, Vector3.right );
        List<List<GameObject>> ys = controller.sortCubes( cubeUnits, Vector3.up );
        List<List<GameObject>> zs = controller.sortCubes( cubeUnits, Vector3.forward );

        controller.createPlaneController( xs, magicCube, "x", MagicCubeContoller.xPlaneTag );
        controller.createPlaneController( ys, magicCube, "y", MagicCubeContoller.yPlaneTag );
        controller.createPlaneController( zs, magicCube, "z", MagicCubeContoller.zPlaneTag );

        PrefabUtility.SaveAsPrefabAsset( magicCube, "Assets/Prefabs/MagicCube.prefab" );
    }

    static List<GameObject> m_gameObjects;
    static void loadMeshes()
    {
        m_gameObjects = new List<GameObject>();
        string[] names = {
            "ArtAssets/Mesh/SM_Box01",
            "ArtAssets/Mesh/SM_Box02",
            "ArtAssets/Mesh/SM_Box03",
            "ArtAssets/Mesh/SM_Box04",
            "ArtAssets/Mesh/SM_Box05",
            "ArtAssets/Mesh/SM_Box06",
        };

        foreach( string name in names )
        {
            GameObject go = Resources.Load<GameObject>(name);
            m_gameObjects.Add( go );
        }
    }

    static GameObject getRandomGameObject()
    {
        int nIndex = Random.Range(0, m_gameObjects.Count);
        return Instantiate(m_gameObjects[nIndex]);
    }

    static Vector3 getRandomAngle()
    {
        Vector3[] angles = {
            new Vector3(0,0,0),
            new Vector3(0,90,0),
            new Vector3(0,0,90),
            new Vector3(90,0,0),
            new Vector3(90,90,0),
            new Vector3(90,0,90),
            new Vector3(0,90,90),
            new Vector3(90,90,90),
        };
        int nIndex = Random.Range(0, angles.Length);
        return angles[nIndex];
    }


    static GameObject generateCubes( float size, float margin, int dimension, out List<GameObject> rst )
    {
        rst = new List<GameObject>();
        // Mesh mesh = Resources.GetBuiltinResource<Mesh>("Cube.fbx");
        Material footshadow_material = Resources.Load<Material>("Materials/footshadow_material");
        Material flash_material = Resources.Load<Material>("Materials/flash_material");
        Material mousepick_material = Resources.Load<Material>("Materials/mousepick_material");
        int index = 0;

        GameObject magicCubeGo = new GameObject();
        magicCubeGo.name = "MagicCube";
        GameObject unitsGo = new GameObject();
        unitsGo.name = "CubeUnits";
        unitsGo.transform.SetParent( magicCubeGo.transform );

        for( int x=0; x<dimension; ++x )
        {
            for( int y=0; y<dimension; ++y )
            {
                for( int z=0; z<dimension; ++z )
                {
                    float coordX = x * (size + margin);
                    float coordY = y * (size + margin);
                    float coordZ = z * (size + margin);
                    // GameObject go = new GameObject();
                    GameObject go = getRandomGameObject();
                    go.name = "CubeUnit." + index++;
                    go.transform.position = new Vector3( coordX, coordY, coordZ );
                    go.transform.localEulerAngles = getRandomAngle();
                    go.layer = 3;
                    // go.AddComponent<MeshFilter>().mesh = getMesh();
                    // go.AddComponent<MeshRenderer>();
                    // go.GetComponent<MeshRenderer>().material = normal_material;
                    go.AddComponent<BoxCollider>();
                    go.AddComponent<MagicCubeUnit>();
                    HighLightBeats beats = go.AddComponent<HighLightBeats>();
                    // beats.m_activeMaterial = active_material;
                    // beats.m_normalMaterial = normal_material;
                    beats.m_footShadowMaterial = footshadow_material;
                    beats.m_flashMaterial = flash_material;
                    beats.m_mousePickMaterial = mousepick_material;

                    go.transform.SetParent( unitsGo.transform );
                    rst.Add( go );
                }
            }
        }

        return magicCubeGo;
    }


}
#endif
