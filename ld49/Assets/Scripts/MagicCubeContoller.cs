using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class MagicCubeContoller : MonoBehaviour
{
    public enum DecreaseDimensionState
    {
        None,
        Begin,
        Flashing,
    };
    public static string xPlaneTag = "XPlaneController";
    public static string yPlaneTag = "YPlaneController";
    public static string zPlaneTag = "ZPlaneController";
    public int mDecreaseFlashCount = 4;
    public float mDecreaseFlashTimeElapse = 0.5f;
    public DecreaseDimensionState m_ddState = DecreaseDimensionState.None; // set to Begin, start DecreaseDimension

    int mPrepareCount = 0;
    float prepareElapse = 0f;
    
    public delegate void OnDecreaseDimensionCallBack();
    public OnDecreaseDimensionCallBack onDecreaseDimensionCallBack;
    public void Update()
    {
        if( DecreaseDimensionState.Begin == m_ddState )
        {
            prepareDecreaseDimension();
            m_ddState = DecreaseDimensionState.Flashing;
            prepareElapse = 0f;
            mPrepareCount = 0;
        }
        else if( DecreaseDimensionState.Flashing == m_ddState )
        {
            prepareElapse += Time.deltaTime;
            // Debug.Log( "prepareElapse = " + prepareElapse );
            if( prepareElapse > mDecreaseFlashTimeElapse )
            {
                // Debug.Log( "prepareDecreaseDimension" );

                changeHighLightBeats(m_xs[m_xIndex], m_isActive, HighLightBeats.HighlightMode.Flash);
                changeHighLightBeats(m_ys[m_yIndex], m_isActive, HighLightBeats.HighlightMode.Flash);
                changeHighLightBeats(m_zs[m_zIndex], m_isActive, HighLightBeats.HighlightMode.Flash);
                m_isActive = !m_isActive;
                // change material
                prepareElapse = 0f;
                mPrepareCount++;
                
                if( mPrepareCount > mDecreaseFlashCount )
                {
                    decreaseDimension();
                    m_ddState = DecreaseDimensionState.None;
                }
            }
        }
    }
    bool m_isActive = false;
    List<List<GameObject>> m_xs;
    List<List<GameObject>> m_ys;
    List<List<GameObject>> m_zs;
    int m_xIndex;
    int m_yIndex;
    int m_zIndex;

    public Vector3 getCenter()
    {
        List<GameObject> units = findCubeUnits();
        Vector3 center = Vector3.zero;
        for( int j=0; j<units.Count; ++j )
        {
            center += units[j].transform.position;
        }
        center /= units.Count;

        List<List<GameObject>> ys = sortCubes( units, Vector3.up );
        float y = ys[ys.Count/2][0].transform.position.y;
        List<List<GameObject>> xs = sortCubes( ys[0], Vector3.right );
        float x = xs[xs.Count/2][0].transform.position.x;
        List<List<GameObject>> zs = sortCubes( xs[0], Vector3.forward );
        float z = zs[zs.Count/2][0].transform.position.z;
        return new Vector3(x, y, z);
    }

    public List<GameObject> findCubeUnits()
    {
        List<GameObject> result = new List<GameObject>();
        MagicCubeUnit[] units = gameObject.GetComponentsInChildren<MagicCubeUnit>();
        for( int j=0; j<units.Length; ++j )
        {
            result.Add( units[j].gameObject );
        }
        return result;
    }

    public List<GameObject> findPlaneControllers(string tag)
    {
        List<GameObject> result = new List<GameObject>();
        MagicCubePlaneContoller[] planeControllers = gameObject.GetComponentsInChildren<MagicCubePlaneContoller>();
        for( int j=0; j<planeControllers.Length; ++j )
        {
            if( planeControllers[j].gameObject.CompareTag(tag) )
            {
                result.Add( planeControllers[j].gameObject );
            }
        }
        return result;
    }

    public void updatePlaneControllers()
    {
        List<GameObject> cubeUnits = findCubeUnits();
        if( 0==cubeUnits.Count )
        {
            return;
        }
        List<List<GameObject>> xs = sortCubes( cubeUnits, Vector3.right );
        List<List<GameObject>> ys = sortCubes( cubeUnits, Vector3.up );
        List<List<GameObject>> zs = sortCubes( cubeUnits, Vector3.forward );

        clearPlaneControllerGOs();
        createPlaneController( xs, gameObject, "x", MagicCubeContoller.xPlaneTag );
        createPlaneController( ys, gameObject, "y", MagicCubeContoller.yPlaneTag );
        createPlaneController( zs, gameObject, "z", MagicCubeContoller.zPlaneTag );

    }

    public void prepareDecreaseDimension()
    {
        List<GameObject> cubeUnits = findCubeUnits();
        if( 0==cubeUnits.Count )
        {
            return;
        }
        m_xs = sortCubes( cubeUnits, Vector3.right );
        m_ys = sortCubes( cubeUnits, Vector3.up );
        m_zs = sortCubes( cubeUnits, Vector3.forward );

        m_xIndex = Random.Range(0, 2) * (m_xs.Count-1);
        m_yIndex = 0;//Random.Range(0, 2) * (m_ys.Count-1);
        m_zIndex = Random.Range(0, 2) * (m_zs.Count-1);

        //mInPrepareDecreaseDimension = true;
    }

    public void decreaseDimension()
    {
        destroyGameobjects( m_xs[m_xIndex] );
        destroyGameobjects( m_ys[m_yIndex] );
        destroyGameobjects( m_zs[m_zIndex] );
        m_xs = null;
        m_ys = null;
        m_zs = null;

        List<GameObject> cubeUnits = findCubeUnits();
        if( 0==cubeUnits.Count )
        {
            return;
        }

        List<List<GameObject>> xs = sortCubes( cubeUnits, Vector3.right );
        List<List<GameObject>> ys = sortCubes( cubeUnits, Vector3.up );
        List<List<GameObject>> zs = sortCubes( cubeUnits, Vector3.forward );

        // Debug.Log( "xs = " + xs.Count + ", ys = " + ys.Count + ", zs = " + zs.Count);
        clearPlaneControllerGOs();
        createPlaneController( xs, gameObject, "x", MagicCubeContoller.xPlaneTag );
        createPlaneController( ys, gameObject, "y", MagicCubeContoller.yPlaneTag );
        createPlaneController( zs, gameObject, "z", MagicCubeContoller.zPlaneTag );

        onDecreaseDimensionCallBack();
    }

    public List<List<GameObject>> sortCubes( List<GameObject> cubeUnits, Vector3 axis )
    {
        List<List<GameObject>> result = new List<List<GameObject>> ();
        List<GameObject> currentGroup = null;
        float lastValue = float.MaxValue;
        float threshold = 0.1f;
        if( axis.x != 0 )
        {
            cubeUnits.Sort((lhs, rhs) =>
            {
                return lhs.transform.position.x < rhs.transform.position.x ? -1 : 1;
            });

            for( int j=0; j<cubeUnits.Count; ++j )
            {
                if( Mathf.Abs(lastValue - cubeUnits[j].transform.position.x) > threshold )
                {
                    currentGroup = new List<GameObject>();
                    result.Add( currentGroup );

                    lastValue = cubeUnits[j].transform.position.x;
                }

                currentGroup.Add( cubeUnits[j] );
            }
        }
        else if( axis.y != 0 )
        {
            cubeUnits.Sort((lhs, rhs) =>
            {
                return lhs.transform.position.y < rhs.transform.position.y ? -1 : 1 ;
            });
            for( int j=0; j<cubeUnits.Count; ++j )
            {
                if( Mathf.Abs(lastValue - cubeUnits[j].transform.position.y) > threshold )
                {
                    currentGroup = new List<GameObject>();
                    result.Add( currentGroup );

                    lastValue = cubeUnits[j].transform.position.y;
                }

                currentGroup.Add( cubeUnits[j] );
            }
        }
        else if( axis.z != 0 )
        {
            cubeUnits.Sort((lhs, rhs) =>
            {
                return lhs.transform.position.z < rhs.transform.position.z ? -1 : 1 ;
            });
            for( int j=0; j<cubeUnits.Count; ++j )
            {
                if( Mathf.Abs(lastValue - cubeUnits[j].transform.position.z) > threshold )
                {
                    currentGroup = new List<GameObject>();
                    result.Add( currentGroup );

                    lastValue = cubeUnits[j].transform.position.z;
                }

                currentGroup.Add( cubeUnits[j] );
            }
        }
        return result;
    }

    void clearPlaneControllerGOs()
    {
        MagicCubePlaneContoller[] controllers = gameObject.GetComponentsInChildren<MagicCubePlaneContoller>();
        for( int j=0; j<controllers.Length; ++j )
        {
            DestroyImmediate( controllers[j].gameObject );
        }
    }

    void destroyGameobjects( List<GameObject> objects)
    {
        foreach( GameObject item in objects )
        {
            DestroyImmediate( item );
        }
    }

    public void changeHighLightBeats( List<GameObject> objects, bool isActive, HighLightBeats.HighlightMode hightLightMode)
    {
        for( int j=0; j<objects.Count; ++j )
        {
            HighLightBeats beats = objects[j].GetComponent<HighLightBeats>();
            beats.m_hightLightMode = hightLightMode;
            beats.IsActive = isActive;
        }
    }

    public void changeHighLightBeats( GameObject goObject, bool isActive, HighLightBeats.HighlightMode hightLightMode)
    {
        HighLightBeats beats = goObject.GetComponent<HighLightBeats>();
        beats.m_hightLightMode = hightLightMode;
        beats.IsActive = isActive;
    }

    public void createPlaneController( List<List<GameObject>> items, GameObject parent, string namePrefix, string tag )
    {
        for( int j=0; j<items.Count; ++j )
        {
            GameObject go = new GameObject();
            go.name = namePrefix + j;
            go.tag = tag;
            go.transform.SetParent( parent.transform );
            MagicCubePlaneContoller com = go.AddComponent<MagicCubePlaneContoller>();
            com.m_childrenObjects = items[j];
            com.setupCenter();
        }
    }

    public List<MagicCubePlaneContoller> getPlaneControllersByUnit( GameObject unit )
    {
        List<MagicCubePlaneContoller> result = new List<MagicCubePlaneContoller>();
        MagicCubePlaneContoller[] planeControllers = gameObject.GetComponentsInChildren<MagicCubePlaneContoller>();
        for( int j=0; j<planeControllers.Length; ++j )
        {
            List<GameObject> units = planeControllers[j].m_childrenObjects;
            for( int k=0; k<units.Count; ++k )
            {
                if( unit == units[k] )
                {
                    result.Add( planeControllers[j] );
                    break;
                }
            }
        }
        return result;
    }

    //List<MagicCubePlaneContoller> planeController = m_magicCubeController.getPlaneControllersByUnit( gameObj );
    public MagicCubePlaneContoller getPlaneControllerByTag( List<MagicCubePlaneContoller> planeControllers, string tag )
    {
        for( int j=0; j<planeControllers.Count; ++j )
        {
            if( planeControllers[j].CompareTag(tag) )
            {
                return planeControllers[j];
            }
        }
        return null;
    }

    Vector2 normalizeDir( Vector2 dir )
    {
        float angle = Vector2.SignedAngle(dir, Vector2.right) + 180.0f;
        if( angle < 45 || angle > 315 )
        {
            return Vector2.right;
        }
        else if( angle >= 45 && angle < 135 )
        {
            return Vector2.up;
        }
        else if( angle >= 135 && angle < 225 )
        {
            return Vector2.left;
        }
        else// if( angle >= 135 && angle < 225 )
        {
            return Vector2.down;
        }
    }

    public Vector2 getDir( Vector3 normal, Vector3 deltaOffset )
    {
        if( normal.x > 0.5 )
        {
            return normalizeDir( new Vector2(deltaOffset.z, deltaOffset.y) );
        }
        else if( normal.y > 0.5 )
        {
            return normalizeDir( new Vector2(deltaOffset.z, deltaOffset.x) );
        }
        else if( normal.z < -0.5 )
        {
            return normalizeDir( new Vector2(deltaOffset.x, deltaOffset.y) );
        }
        return Vector2.zero;
    }

    public bool getRotateAngle( List<MagicCubePlaneContoller> planeControllers, Vector3 normal, Vector2 dir, out MagicCubePlaneContoller planeController, out Vector3 deltaAngle )
    {
        planeController = null;
        deltaAngle = Vector3.zero;

        if( normal.x > 0.5 )
        {
            if( dir.x > 0.5 )
            {
                // y, 90
                planeController = getPlaneControllerByTag( planeControllers, yPlaneTag );
                deltaAngle = new Vector3(0, 90, 0);
                return true;
            }
            else if( dir.x < -0.5 )
            {
                // y, -90
                planeController = getPlaneControllerByTag( planeControllers, yPlaneTag );
                deltaAngle = new Vector3(0, -90, 0);
                return true;
            }
            else if( dir.y > 0.5 )
            {
                // z, -90
                planeController = getPlaneControllerByTag( planeControllers, zPlaneTag );
                deltaAngle = new Vector3(0, 0, 90);
                return true;
            }
            else if( dir.y < -0.5 )
            {
                // z, 90
                planeController = getPlaneControllerByTag( planeControllers, zPlaneTag );
                deltaAngle = new Vector3(0, 0, -90);
                return true;
            }
        }
        else if( normal.y > 0.5 )
        {
            if( dir.x > 0.5 )
            {
                // x, 90
                planeController = getPlaneControllerByTag( planeControllers, xPlaneTag );
                deltaAngle = new Vector3(-90, 0, 0);
                return true;
            }
            else if( dir.x < -0.5 )
            {
                // x, -90
                planeController = getPlaneControllerByTag( planeControllers, xPlaneTag );
                deltaAngle = new Vector3(90, 0, 0);
                return true;
            }
            else if( dir.y > 0.5 )
            {
                // z, -90
                planeController = getPlaneControllerByTag( planeControllers, zPlaneTag );
                deltaAngle = new Vector3(0, 0, -90);
                return true;
            }
            else if( dir.y < -0.5 )
            {
                // z, 90
                planeController = getPlaneControllerByTag( planeControllers, zPlaneTag );
                deltaAngle = new Vector3(0, 0, 90);
                return true;
            }
        }
        else if( normal.z < -0.5 )
        {
            if( dir.x > 0.5 )
            {
                // y, 90
                planeController = getPlaneControllerByTag( planeControllers, yPlaneTag );
                deltaAngle = new Vector3(0, 90, 0);
                return true;
            }
            else if( dir.x < -0.5 )
            {
                // y, -90
                planeController = getPlaneControllerByTag( planeControllers, yPlaneTag );
                deltaAngle = new Vector3(0, -90, 0);
                return true;
            }
            else if( dir.y > 0.5 )
            {
                // x, -90
                planeController = getPlaneControllerByTag( planeControllers, xPlaneTag );
                deltaAngle = new Vector3(90, 0, 0);
                return true;
            }
            else if( dir.y < -0.5 )
            {
                // x, 90
                planeController = getPlaneControllerByTag( planeControllers, xPlaneTag );
                deltaAngle = new Vector3(-90, 0, 0);
                return true;
            }
        }
        return false;
    }
}
