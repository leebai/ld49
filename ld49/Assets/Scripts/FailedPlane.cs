using UnityEngine;

public class FailedPlane : MonoBehaviour
{
    public MyLevelManager m_levelManager = null;
    MagicCubeContoller m_magicCubeController;

    MagicCubeContoller getCubeController()
    {
        if( null == m_magicCubeController )
        {
            m_magicCubeController = GameObject.Find("MagicCube").GetComponent<MagicCubeContoller>();
        }
        return m_magicCubeController;
    }


   private void OnTriggerEnter(Collider other)
    {
        if( other.gameObject.CompareTag( "Player" ) )
        {
            other.gameObject.GetComponent<PlayerController>().m_isPressForward = false;
            other.gameObject.GetComponent<CharacterController>().enabled = false;
            other.gameObject.transform.position = getCubeController().getCenter() + new Vector3(0,10,0);
            other.gameObject.GetComponent<CharacterController>().enabled = true;
            other.gameObject.GetComponent<CharacterController>().SimpleMove(Vector3.zero);
            // Debug.Log("OnTriggerEnter : " + other.gameObject.transform.position);
            // Update 
            m_levelManager.clearLevelResources();
            m_levelManager.OnLevelStart();

        }
    }
}
