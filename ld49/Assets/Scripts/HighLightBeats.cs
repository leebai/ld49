using UnityEngine;

public class HighLightBeats : MonoBehaviour
{
    public enum HighlightMode
    {
        FootShadow,
        Flash,
        MousePick,
    };

    bool m_isActive;
    public bool IsActive
    {
        get
        {
            return m_isActive;
        }
        set
        {
            m_isActive = value;
            if( m_isActive )
            {
                MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
                // Debug.Log( "active = true, " + renderer.materials.Length );
                Material[] materials = renderer.materials;
                
                if( HighlightMode.FootShadow == m_hightLightMode )
                {
                    materials[0] = m_footShadowMaterial;
                }
                else if( HighlightMode.Flash == m_hightLightMode )
                {
                    materials[0] = m_flashMaterial;
                }
                else if( HighlightMode.MousePick == m_hightLightMode )
                {
                    materials[0] = m_mousePickMaterial;
                }
            
                renderer.materials = materials;
            }
            else
            {
                MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
                // Debug.Log( "active = false, " + renderer.materials.Length );
                Material[] materials = renderer.materials;
                materials[0] = m_flashMaterial;
                materials[0] = m_originMaterial;
                renderer.materials = materials;
            }
        }
    }

    public HighlightMode m_hightLightMode = HighlightMode.FootShadow;

    public Material m_originMaterial;
    public Material m_footShadowMaterial; // foot
    public Material m_flashMaterial;
    public Material m_mousePickMaterial;

    void Awake()
    {
        MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
        Material[] materials = renderer.materials;
        m_originMaterial = materials[0];
        // Debug.Log(m_originMaterial);
        IsActive = false;
    }
};
