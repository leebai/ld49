using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
// using System.Collections;
// using System.Collections.Generic;

public class CreditsPageUI : MonoBehaviour
{
    public GameObject m_return_button = null;
    public SceneSwitcher m_scene_switcher = null;

    void Start()
    {
        Button btn = m_return_button.GetComponent<Button>();
        btn.onClick.AddListener( onReturnClick );
    }

    void onReturnClick()
    {
        m_scene_switcher.m_scene_name = "StartPage";
        StartCoroutine(m_scene_switcher.LoadYourAsyncScene());
    }
}
