using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    public float maxAngleSpeed = 400;
    public float minAngleSpeed = 100;
    public float acceleration = 40;
    public float maxMoveSpeed = 10f;
    public float jumpSpeed = 10f;
    public float gravity = 10f;

    public Transform cameraTransform;
    public GameObject m_unitOne;
    public GameObject m_unitTwo;
    public float m_movingElapseThreshold = 0.7f;

    [FMODUnity.EventRef]
    public string m_turnEvent = "";
    FMOD.Studio.EventInstance m_turnState;

    bool isOnGround;
    bool isSecondJump;
    float ySpeed;
    float moveSpeed;
    Vector3 input;
    Vector3 move;
    CharacterController cont;

    bool m_isMoving = false;
    public bool m_isPressForward = false;
    
    float m_gridDistance = 0.529f; // [MUST] two grid distance
    float m_alignRadiusThreshold = 0.529f/2.0f+0.1f;  // [MUST] a little bit more than half of two grid distance
    GameObject m_alignTargetGameObject = null;
    GameObject m_lastAlignTargetGameObject = null;
    Vector3 m_startPosition;
    float m_movingElapse = 0;
    MagicCubeContoller m_cubeController = null;

    MagicCubeContoller getCubeController()
    {
        if( null==m_cubeController )
        {
            m_cubeController = GameObject.Find("MagicCube").GetComponent<MagicCubeContoller>();
        }
        return m_cubeController;
    }

    void Start()
    {
        m_movingElapse = 0;
        cont = this.GetComponent<CharacterController>();
        m_gridDistance = (m_unitOne.transform.position - m_unitTwo.transform.position).magnitude;
        m_alignRadiusThreshold = m_gridDistance/2.0f+0.1f;  // [MUST] a little bit more than half of two grid distance

        Debug.Log(m_turnEvent);
        m_turnState = FMODUnity.RuntimeManager.CreateInstance(m_turnEvent);
    }


    void Update()
    {
        if( Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.Space))
        {
            m_isPressForward = true;
        }

        //Debug.Log( "localEulerAngles = " + gameObject.transform.localEulerAngles );
        // update direction
        if( Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.A))
        {
            // Debug.Log( "Left..." );
            gameObject.transform.localEulerAngles = new Vector3( gameObject.transform.localEulerAngles.x, gameObject.transform.localEulerAngles.y - 90, gameObject.transform.localEulerAngles.z );
            m_turnState.start();
        }
        else if( Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.D))
        {
            // Debug.Log( "Right ... " );
            gameObject.transform.localEulerAngles = new Vector3( gameObject.transform.localEulerAngles.x, gameObject.transform.localEulerAngles.y + 90, gameObject.transform.localEulerAngles.z );
            m_turnState.start();
        }

        // if( !cont.isGrounded )
        // {
        //     Debug.Log( cont.isGrounded );
        // }
        if( m_isMoving )
        {
            isOnGround = cont.isGrounded;
            Drop();
            // Jump();
            // Turn();
            Move();
        }
        else
        {
            m_movingElapse += Time.deltaTime;
            if( m_movingElapse > m_movingElapseThreshold )
            {
                // every second, call MoveForward
                m_isMoving = true;
                m_startPosition = gameObject.transform.position;
            }
            else
            {
            }
        }
    }

    GameObject getAlignTarget()
    {
        if( null == m_alignTargetGameObject )
        {
            float alignRadius = (m_startPosition - gameObject.transform.position).magnitude;
            //            Debug.Log( "alignRadius = " + alignRadius + ", m_alignRadiusThreshold = " + m_alignRadiusThreshold );
            if( alignRadius > m_alignRadiusThreshold )
            {
                Vector3 origin = gameObject.transform.position;
                Vector3 direction = Vector3.down;
                RaycastHit hitInfo;
                int layer_mask = LayerMask.GetMask( "Ground" );
                if( Physics.Raycast( origin, direction, out hitInfo, 1, layer_mask ) )
                {
                    m_alignTargetGameObject = hitInfo.collider.gameObject;
                    getCubeController().changeHighLightBeats( m_alignTargetGameObject, true, HighLightBeats.HighlightMode.FootShadow);
                    // HighLightBeats highlightBeats = m_alignTargetGameObject.GetComponent<HighLightBeats>();
                    // if( null != highlightBeats )
                    // {
                    //     highlightBeats.IsActive = true;
                    // }

                    if( null != m_lastAlignTargetGameObject )
                    {
                        getCubeController().changeHighLightBeats( m_lastAlignTargetGameObject, false, HighLightBeats.HighlightMode.FootShadow);
                        // highlightBeats = m_lastAlignTargetGameObject.GetComponent<HighLightBeats>();
                        // if( null != highlightBeats )
                        // {
                        //     highlightBeats.IsActive = false;
                        // }
                    }
                    m_lastAlignTargetGameObject = m_alignTargetGameObject;
                    //Debug.Log( m_alignTargetGameObject.name );
                }
            }
        }
        
        return m_alignTargetGameObject;
    }

    void Move()
    {
        Vector3 forwardDir = gameObject.transform.TransformDirection(-1,0,0);
        // float h = Input.GetAxis("Horizontal");
        // float v = Input.GetAxis("Vertical");
        float h = 0f;
        float v = 0f;
        if( m_isPressForward )
        {
            if( forwardDir.z > 0.5 )
            {
                v = 1f;
            }
            else if( forwardDir.z < -0.5 )
            {
                v = -1f;
            }
            if( forwardDir.x > 0.5 )
            {
                h = 1f;
            }
            else if( forwardDir.x < -0.5 )
            {
                h = -1f;
            }
        }
        // Debug.Log( "forwardDir = " + forwardDir + ", h=" + h + ", v = " + v );
        
        input.x = h;
        input.y = 0;
        input.z = v;
        moveSpeed = Mathf.MoveTowards(moveSpeed, input.normalized.magnitude * maxMoveSpeed, acceleration * Time.deltaTime );
        move = input * Time.deltaTime * moveSpeed;
        // Debug.Log( "\tmove = " + move );
        // //move = this.cameraTransform.TransformDirection(move);
        // Debug.Log( "\tmove2 = " + move );

        // get target not null
        GameObject alignTargetGameObject = getAlignTarget();
        // align position
        if( null!=alignTargetGameObject )
        {
            float distance = ((Vector2)(alignTargetGameObject.transform.position - (gameObject.transform.position + move))).magnitude;
            float threshold = Mathf.Abs( distance - m_gridDistance );
            if( threshold < 1 )
            {
                //Debug.Log( "h = " + h + ", v = " + v + ", alignTargetGameObject = " + alignTargetGameObject.name );
                gameObject.transform.position = new Vector3( alignTargetGameObject.transform.position.x, gameObject.transform.position.y, alignTargetGameObject.transform.position.z );
                m_isMoving = false;
                m_movingElapse = 0;
                m_startPosition = gameObject.transform.position;
                // HighLightBeats highlightBeats = m_alignTargetGameObject.GetComponent<HighLightBeats>();
                // if( null != highlightBeats )
                // {
                //     highlightBeats.IsActive = false;
                // }

                m_alignTargetGameObject = null;
                return;
            }
        }

        // vertical
        move += Vector3.up * ySpeed * Time.deltaTime;
        // Debug.Log( "\tmove = " + move + ", position = " + transform.position );
        cont.Move(move);
        // Debug.Log( "\t\tmove = " + move + ", position = " + transform.position );

    }
    void Drop()
    {
        if( !isOnGround )
        {
            ySpeed -= gravity * Time.deltaTime;
        }
        else
        {
            if( ySpeed < -1 )
            {
                ySpeed += gravity * Time.deltaTime;
            }

            if(isSecondJump)
            {
                isSecondJump = false;
            }
        }
    }

    void Jump()
    {
        if( isOnGround )
        {
            if( Input.GetButtonDown("Jump"))
            {
                ySpeed = jumpSpeed;
            }
        }
        else
        {
            if(!isSecondJump)
            {
                if( Input.GetButtonDown("Jump"))
                {
                    ySpeed += jumpSpeed;
                    isSecondJump = true;
                }
            }
        }
    }

    void Turn()
    {
        if( input.x != 0 || input.z != 0 )
        {
            Vector3 targetDirection = cameraTransform.TransformDirection(input);
            targetDirection.y = 0;
            Quaternion lookQuaternion = Quaternion.LookRotation(targetDirection);

            float turnSpeed = Mathf.Lerp(minAngleSpeed, maxAngleSpeed, moveSpeed / maxMoveSpeed);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, lookQuaternion, turnSpeed * Time.deltaTime);
        }
    }

};
