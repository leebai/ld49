using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

// using System.Collections;
// using System.Collections.Generic;

public class EndingPage : MonoBehaviour
{
    public VideoPlayer m_videoPlayer;
    public SceneSwitcher m_scene_switcher = null;
    private void Awake()
    {
        m_videoPlayer.loopPointReached += test; //设置委托
    }

    void test(VideoPlayer video)
    {
        m_scene_switcher.m_scene_name = "StartPage";
        StartCoroutine(m_scene_switcher.LoadYourAsyncScene());
    }
}

