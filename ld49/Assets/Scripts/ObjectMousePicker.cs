using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectMousePicker : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string m_cubeRotateEvent = "";
    FMOD.Studio.EventInstance m_cubeRotateState;

    public PlayerController m_playerController;

    MagicCubeContoller m_magicCubeController;
    Vector3 m_pivortPoint;
    List<MagicCubePlaneContoller> m_planeControllers;
    GameObject m_pivotGameObject;
    bool m_rotateLock = false;

    MagicCubeContoller getMagicCubeController()
    {
        if( null==m_magicCubeController )
        {
            m_magicCubeController = GameObject.Find("MagicCube").GetComponent<MagicCubeContoller>();
        }
        return m_magicCubeController;
    }

    Vector3 m_lastestNormal;
    Vector3 m_lastestPoint;
    bool m_isRotateOperation = false;

    void Start()
    {
        m_cubeRotateState = FMODUnity.RuntimeManager.CreateInstance(m_cubeRotateEvent);
    }
    void Update ()
    {
        if( MagicCubeContoller.DecreaseDimensionState.None != getMagicCubeController().m_ddState )
        {
            return;
        }
        if( m_rotateLock )
        {
            return;
        }

        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            int layer_mask = LayerMask.GetMask( "Ground" );
            if(Physics.Raycast(ray,out hitInfo, Mathf.Infinity, layer_mask))
            {
                m_pivotGameObject = hitInfo.collider.gameObject;
                m_pivortPoint = hitInfo.point;
                m_planeControllers = getMagicCubeController().getPlaneControllersByUnit( m_pivotGameObject );

                m_lastestNormal = hitInfo.normal;
                m_isRotateOperation = true;
                // // create a mesh
                // Debug.Log("click object name is " + m_pivotGameObject.name + ", normal = " + hitInfo.normal + ", point = " + hitInfo.point + "++++++++++++++++++++");
                // 

                getMagicCubeController().changeHighLightBeats( m_pivotGameObject, true, HighLightBeats.HighlightMode.MousePick);
            }
        }
        else if(Input.GetMouseButtonUp(0))
        {
            if( !m_isRotateOperation )
            {
                return;
            }

            updateLatestPoint();

            Vector3 deltaOffset = m_lastestPoint - m_pivortPoint;
            Vector2 dir = getMagicCubeController().getDir( m_lastestNormal, deltaOffset );
            Vector3 deltaAngle;
            MagicCubePlaneContoller planeController;
            if( getMagicCubeController().getRotateAngle( m_planeControllers, m_lastestNormal, dir, out planeController, out deltaAngle ) )
            {
                m_rotateLock = true;
                // Debug.Log( "onRotateFinishedCallBack++++++++++++++++++++" );

                planeController.onRotateFinishedCallBack = onRotateFinishedCallBack;
                // Debug.Log("click object name is " + planeController.gameObject.name + ", angle = " + deltaAngle + ", deltaOffset = " + deltaOffset + "--------------------");
                planeController.IsActive = true;
                m_playerController.m_isPressForward = true;
                planeController.m_deltaAngle = deltaAngle;
                // planeController.gameObject.transform.localEulerAngles = planeController.gameObject.transform.localEulerAngles + deltaAngle;
                getMagicCubeController().changeHighLightBeats( m_pivotGameObject, false, HighLightBeats.HighlightMode.MousePick);
                m_pivotGameObject = null;
                m_cubeRotateState.start();
                Debug.Log("m_cubeRotateState =" + m_cubeRotateState + "====================");
            }
            m_isRotateOperation = false;
        }
        else if(Input.GetMouseButton(0))
        {
            if( !m_isRotateOperation )
            {
                return;
            }

            updateLatestPoint();
        }
    }

    void updateLatestPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        int layer_mask = LayerMask.GetMask( "Ground" );
        if(Physics.Raycast(ray,out hitInfo, Mathf.Infinity, layer_mask))
        {
            if( m_lastestNormal.Equals(hitInfo.normal) )
            {
                m_lastestPoint = hitInfo.point;
            }
        }
    }

    void onRotateFinishedCallBack()
    {
        m_rotateLock = false;
    }

}
