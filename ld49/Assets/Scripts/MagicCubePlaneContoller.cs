using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MagicCubePlaneContoller : MonoBehaviour
{
    public List<GameObject> m_childrenObjects = new List<GameObject>();
    public float m_rotateAnimationTime = 0.5f;

    bool m_isActive;
    public bool IsActive
    {
        get
        {
            return m_isActive;
        }
        set
        {
            m_isActive = value;
            if( m_isActive )
            {
                beginRotate();
                m_animationElapse = 0;
                m_originAngle = gameObject.transform.localEulerAngles;
            }
            else
            {
                endRotate();
                // 
            }
        }
    }

    public Vector3 m_deltaAngle;

    public delegate void OnRotateFinishedCallBack();
    public OnRotateFinishedCallBack onRotateFinishedCallBack;

    Vector3 m_originAngle;
    float m_animationElapse;
    void Update()
    {
        if( !IsActive )
        {
            return;
        }

        m_animationElapse += Time.deltaTime;
        if( m_animationElapse > m_rotateAnimationTime )
        {
            gameObject.transform.localEulerAngles = m_originAngle + m_deltaAngle;
            IsActive = false;
            MagicCubeContoller controller = gameObject.transform.parent.gameObject.GetComponent<MagicCubeContoller>();
            controller.updatePlaneControllers();
            onRotateFinishedCallBack();
        }
        else
        {
            gameObject.transform.localEulerAngles = m_originAngle + m_animationElapse * m_deltaAngle / m_rotateAnimationTime;
        }
    }

    List<GameObject> m_parentsObjects = new List<GameObject>();
    public void Awake()
    {
        // setupCenter();
        // IsActive = true;
    }

    public void setupCenter()
    {
        Vector3 center = Vector3.zero;
        for( int j=0; j<m_childrenObjects.Count; ++j )
        {
            center += m_childrenObjects[j].transform.position;
        }
        center /= m_childrenObjects.Count;
        // Debug.Log(center);
        transform.position = center;
    }

    void beginRotate()
    {
        m_parentsObjects.Clear();
        for( int j=0; j<m_childrenObjects.Count; ++j )
        {
            m_childrenObjects[j].layer = 8;
            m_parentsObjects.Add( m_childrenObjects[j].transform.parent.gameObject );
            m_childrenObjects[j].transform.SetParent(gameObject.transform);
        }
    }

    void endRotate()
    {
        for( int j=0; j<m_childrenObjects.Count; ++j )
        {
            m_childrenObjects[j].layer = 3;
            m_childrenObjects[j].transform.SetParent(m_parentsObjects[j].transform);
        }
    }
};



