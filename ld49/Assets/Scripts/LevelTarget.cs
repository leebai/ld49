using UnityEngine;

public class LevelTarget : MonoBehaviour
{
    public MyLevelManager m_levelManager;
    [FMODUnity.EventRef]
    public string m_touchEvent = "event:/接触目标";
    FMOD.Studio.EventInstance m_touchState;

    void Start()
    {
        m_touchState = FMODUnity.RuntimeManager.CreateInstance(m_touchEvent);
    }

    MyLevelManager getLevelManager()
    {
        if( null==m_levelManager )
        {
            m_levelManager = GameObject.Find("LevelManager").GetComponent<MyLevelManager>();
        }
        return m_levelManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        getLevelManager().OnLevelFinished();
        m_touchState.start();
    }
}
