using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MyLevelManager : MonoBehaviour
{
    public SceneSwitcher m_scene_switcher = null;
    [FMODUnity.EventRef]
    public string m_firstLevelEvent = "";
    FMOD.Studio.EventInstance m_firstLevelState;
    [FMODUnity.EventRef]
    public string m_restLevelEvent = "";
    FMOD.Studio.EventInstance m_restLevelState;

    MagicCubeContoller m_cubeController = null;

    void Awake()
    {
        m_firstLevelState = FMODUnity.RuntimeManager.CreateInstance(m_firstLevelEvent);
        m_restLevelState = FMODUnity.RuntimeManager.CreateInstance(m_restLevelEvent);

        getCubeController().onDecreaseDimensionCallBack = onDecreaseDimensionFinished;

        OnLevelStart();
    }
    
    MagicCubeContoller getCubeController()
    {
        if( null == m_cubeController )
        {
            m_cubeController = GameObject.Find("MagicCube").GetComponent<MagicCubeContoller>();
        }
        return m_cubeController;
    }

    int m_currentLevel = 0;
    int getLevel()
    {
        return m_currentLevel;
    }

    void increaseLevel()
    {
        m_currentLevel++;
    }

    List<GameObject> getKeys( int level )
    {
        if( null==m_keys )
        {
            m_keys = new List<GameObject>();
        }
        else
        {
            m_keys.Clear();
        }
        string[] names = {
            "ArtAssets/Mesh/Level_01/SM_Stump",
            "ArtAssets/Mesh/Level_02/SM_Vent",
            "ArtAssets/Mesh/Level_03/SM_Beaker",
            "ArtAssets/Mesh/Level_04/SM_Outfall",
        };
        int nCount = Random.Range(1, 5);
        GameObject go = Resources.Load<GameObject>(names[level]);
        Debug.Log( "level = " + level + ", "  + names[level] );
        for( int j=0; j<nCount; ++j )
        {
            m_keys.Add( Instantiate(go) );
        }
        return m_keys;
        // return go;
    }

    GameObject getTarget( int level )
    {
        string[] names = {
            "ArtAssets/Mesh/Level_01/SM_Building04",
            "ArtAssets/Mesh/Level_02/SM_Building02",
            "ArtAssets/Mesh/Level_03/SM_Building03",
            "ArtAssets/Mesh/Level_04/SM_Building01",
        };

        m_target = Instantiate(Resources.Load<GameObject>(names[level]));
        m_target.AddComponent<BoxCollider>().isTrigger = true;
        m_target.AddComponent<LevelTarget>();
        return m_target;
    }

    List<GameObject> m_keys;
    GameObject m_target;

    void placeKeys( int level )
    {
        List<GameObject> keys = getKeys( level );
        placeItems( keys );
    }

    void placeTarget( int level )
    {
        GameObject target = getTarget( level );
        placeItem( target );
    }

    void placeItem( GameObject item )
    {
        // find outter cubeUnits
        List<GameObject> xs = m_cubeController.findPlaneControllers( MagicCubeContoller.xPlaneTag );
        List<GameObject> ys = m_cubeController.findPlaneControllers( MagicCubeContoller.yPlaneTag );
        List<GameObject> zs = m_cubeController.findPlaneControllers( MagicCubeContoller.zPlaneTag );
        List<GameObject> objs = new List<GameObject>();
        objs.Add( xs[0] );
        objs.Add( xs[xs.Count-1] );
        objs.Add( zs[0] );
        objs.Add( zs[zs.Count-1] );
        objs.Add( ys[ys.Count-1] );
        objs.Add( ys[0] );

        // random pick
        int nIndex = Random.Range(0, 5);
        // get normal & roateAngle
        Vector3 normal = Vector3.zero;
        Vector3 roateAngle = Vector3.zero;
        if( 0 == nIndex )
        {
            normal = new Vector3(-1, 0, 0);
            roateAngle = new Vector3(0, 0, 90);
        }
        else if( 1 == nIndex )
        {
            normal = new Vector3(1, 0, 0);
            roateAngle = new Vector3(0, 0, -90);
        }
        else if( 2 == nIndex )
        {
            normal = new Vector3(0, 0, -1);
            roateAngle = new Vector3(-90, 0, 0);
        }
        else if( 3 == nIndex )
        {
            normal = new Vector3(0, 0, 1);
            roateAngle = new Vector3(90, 0, 0);
        }
        else if( 4 == nIndex )
        {
            normal = new Vector3(0, 1, 0);
        }
        else if( 5 == nIndex )
        {
            normal = new Vector3(0, -1, 0);
        }
        // get anchor
        int nSubIndex = Random.Range(0, objs[nIndex].GetComponent<MagicCubePlaneContoller>().m_childrenObjects.Count);
        GameObject cubeUnitGo = objs[nIndex].GetComponent<MagicCubePlaneContoller>().m_childrenObjects[nSubIndex];
        Vector3 position = cubeUnitGo.transform.position + normal * cubeUnitGo.GetComponent<MeshRenderer>().bounds.size.x * 0.5f;
        // place item as it's child
        item.transform.position = position;
        item.transform.eulerAngles = roateAngle;
        item.transform.SetParent( cubeUnitGo.transform );
        // Debug.Log( "nIndex = " + nIndex + ", nSubIndex = " + nSubIndex + ", " + cubeUnitGo);
    }
    
    void placeItems( List<GameObject> items )
    {
        for( int j=0; j<items.Count; ++j )
        {
            placeItem( items[j] );
        }
    }

    public void OnLevelStart()
    {
        Debug.Log( "OnLevelStart" );
        int currentLevel = getLevel();
        if( 0==currentLevel)
        {
            m_firstLevelState.start();
        }
        else if( 1==currentLevel)
        {
            if( m_firstLevelState.isValid() )
            {
                m_firstLevelState.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                m_firstLevelState.release();
                m_firstLevelState.clearHandle();
            }
            m_restLevelState.start();
        }

        placeKeys( currentLevel );
        placeTarget( currentLevel );
    }

    public void OnLevelFinished()
    {
        Debug.Log( "OnLevelFinished" );
        int currentLevel = getLevel();
        if( currentLevel < 3 )
        {
            getCubeController().m_ddState = MagicCubeContoller.DecreaseDimensionState.Begin;
        }
        else
        {
            m_scene_switcher.m_scene_name = "Ending";
            StartCoroutine(m_scene_switcher.LoadYourAsyncScene());
        }
    }

    public void OnLevelFailed()
    {
        Debug.Log( "OnLevelFailed" );
    }

    public void onDecreaseDimensionFinished()
    {
        // Debug.Log( "====================onDecreaseDimensionFinished====================" );
        clearLevelResources();
        increaseLevel();
        changeBackgroundImage();        
        OnLevelStart();
    }

    public void clearLevelResources()
    {
        for( int j=0; j<m_keys.Count; ++j )
        {
            Destroy(m_keys[j]);
        }
        m_keys.Clear();
        Destroy(m_target);
    }

    public GameObject m_LevelOne_Background;
    public GameObject m_LevelTwo_Background;
    public GameObject m_LevelTwo2_Background;
    public GameObject m_LevelThree_Background;
    public void changeBackgroundImage()
    {
        int currentLevel = getLevel();
        Debug.Log("====================currentLevel = " + currentLevel + "====================");
        if( 1==currentLevel )
        {
            // remove level1 images
            m_LevelOne_Background.SetActive(false);
        }
        else if( 2==currentLevel )
        {
            // remove level2 images
            m_LevelTwo_Background.SetActive(false);
            m_LevelTwo2_Background.SetActive(false);
        }
        else if( 3==currentLevel )
        {
            // remove level3 images
            m_LevelThree_Background.SetActive(false);
        }
        // for( int j=0; j<m_keys.Count; ++j )
        // {
        //     DestroyImmediate(m_keys[j]);
        // }
        // m_keys.Clear();
        // DestroyImmediate(m_target);
    }


}
